﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Account
    {
        public string AccountType { get; set; }
        public void PrintAccountInfo()
        {
            Console.WriteLine("Account Type: {0}", AccountType);
        }
    }
}
