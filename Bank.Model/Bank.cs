﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
   public class Bank
    {
        public string BankName { get; set; }
        public List<Customer> Customers { get; set; }

        public Bank()
        {
            Customers = new List<Customer>();
        }

        public void PrintBankName()
        {
            Console.WriteLine("Bank Name: " + BankName);
        }

        public void PrintBankDetails()
        {
            Console.WriteLine("Bank Name: " + BankName);
            foreach(Customer cust in Customers)
            {
                Console.WriteLine("Customer Name: "+cust.CustomerName);
                Console.WriteLine("Customer Address: " + cust.Address);
            }
        }
    }
}
