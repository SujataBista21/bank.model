﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Customer
    {
        public string CustomerName { get; set; }
        public List<Account> Accounts { get; set; }

        public string Address { get; set; }

        public Customer()
        {
            Accounts = new List<Account>();
        }

        public void PrintCustomerAccounts()
        {
            foreach(Account acc in Accounts)
            {
                Console.WriteLine("Account Type: {0}", acc.AccountType);
            }

        }

        public void PrintCustomerDetails()
        {
            Console.WriteLine("Customer Name: {0}, Customer Addres: {1}", CustomerName, Address);

        }
    }
}
