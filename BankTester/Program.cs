﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Model;

namespace BankTester
{
    class Program
    {
        public static void Main(string[] args)
        {
            Bank.Model.Bank bank = new Bank.Model.Bank();
            bank.BankName = "Chase Bank";
            bank.PrintBankName();

            Customer customer1 = new Customer();
            customer1.CustomerName = "Susan";
            customer1.Address = "Florida";

            bank.Customers.Add(customer1);
            bank.PrintBankDetails();

            Account acc1 = new Account();
            acc1.AccountType = "Checking";
            Account acc2 = new Account();
            customer1.Accounts.Add(acc1);
            acc1.AccountType = "Saving";
            customer1.Accounts.Add(acc2);
            customer1.PrintCustomerAccounts();
    
            Console.ReadKey();
        }
    }
}
